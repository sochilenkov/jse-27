package ru.t1.sochilenkov.tm.repository;

import ru.t1.sochilenkov.tm.model.Project;
import ru.t1.sochilenkov.tm.api.repository.IProjectRepository;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {
}
